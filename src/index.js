import React from 'react'

import A2 from "react-dom";

import A3 from './components/app'

import A4 from "./store/reducers/rootReducer";

import { createStore as A5, applyMiddleware, compose } from "redux";

import { Provider as A6 } from 'react-redux';

import thunk from 'redux-thunk';

import {reduxFirestore, getFirestore} from 'redux-firestore'
import {reactReduxFirebase, getFirebase} from 'react-redux-firebase'

import A7 from "./../src/config/firestoreconfig";




import 'materialize-css/dist/js/materialize'
import './index.scss'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = A5(A4, composeEnhancers(
  applyMiddleware(thunk.withExtraArgument({
    getFirebase, getFirestore
  })),
  reduxFirestore(A7),
  reactReduxFirebase(A7, {
    useFirestoreForProfile: true,
    userProfile: 'Users',
    attachAuthIsReady: true
  })

))

store.firebaseAuthIsReady.then(() =>{
  A2.render(
    <A6
      store = {store}
    >
      <A3 />
    </A6>,
    document.getElementById('app')
  )
})

