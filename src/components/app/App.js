import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { BrowserRouter as A1, Switch, Route } from "react-router-dom";
import A2 from "./../layout/Navbar";
import A5 from "./../dashboard/Dashboard";
import A6 from "./../projects/ProjectDetails";
import A7 from "./../auth/SignIn";
import A8 from "./../auth/SignUp";
import A9 from "./../projects/CreateProject";

// import logo from "./../../assets/img/mario-bg.png";



export default class App extends Component {
  static propTypes = {
    
  }

  render() {
    return (
      <A1>
        <div>
          
          <A2 />

          <Switch>
            <Route 
              exact
              path = '/'
              component = {A5}
            />
            <Route 
              path = '/project/:id'
              component = {A6}
            />
            <Route 
              path = '/signin'
              component = {A7}
            />
            <Route 
              path = '/signup'
              component = {A8}
            />
            <Route 
              path = '/create'
              component = {A9}
            />
          </Switch>
        </div>
        {/* <img src={logo} alt=""/> */}
      </A1>

    )
  }
}
