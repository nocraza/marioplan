import React, { Component } from 'react'

import A1 from "./../helperfunctions/form-onchange";

import "./SignIn.scss";

import {signin as A2} from "./../../store/actions/authActions";

import { connect as A3 } from "react-redux";

import { withRouter } from "react-router";

import { Redirect } from "react-router-dom";

class SignIn extends Component {
  state = {
    email: '',
    password: '',
  }

  handleChange = e => {
    e.preventDefault()

    this.setState(A1(e))
  }
  
  handleSubmit = e => {
    e.preventDefault()
    this.props.signIn(this.state)
    
    // console.log('signed inn=', this.props)
    !this.props.authError && this.props.history.push('/')
    // this.props.history.push('/')
  }
  render() {
    console.log(this.props)
    const {authError } = this.props.auth
    if(this.props.firebase.auth.uid) return <Redirect to = "/" />
    
    return (
      <div className="container">
        <form 
          onSubmit = {this.handleSubmit}
          className = "white"
        >
        <h5 className="grey-text text-darken-3">
          Sign In
        </h5>

        <div className="input-field">
          <label htmlFor="email">Email</label>
          <input 
            type="email"
            onChange = {this.handleChange}
            id = 'email'
          />
        </div>

        <div className="input-field">
          <label htmlFor="password">Password</label>
          <input 
            type="password"
            onChange = {this.handleChange}
            id = 'password'
          />
        </div>
        
        <div className="input-field">
          {/* <button className="btn pink lighten-1 z-depth-0">
          Login</button> */}
          <button className="nes-btn is-primary">
          Login</button>
          <div className="red-text center-align">
          {authError && <p> {authError} </p> }
          </div>
        </div>

        

        </form>
      </div>
    )
  }
}

const mstp = stt => {
  return {
    ...stt
    }
}

const mdtp = d => {
  return {
    signIn: creds => d(A2(creds))
  }
}

export default withRouter(A3(mstp, mdtp)(SignIn))
