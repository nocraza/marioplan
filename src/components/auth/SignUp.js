import React, { Component } from 'react'

import A1 from "./../helperfunctions/form-onchange";

import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { signUp } from "./../../store/actions/authActions";

class SignUp extends Component {
  state = {
    email: '',
    password: '',
    firstName: '',
    lastName: ''
  }

  handleChange = e => {
    e.preventDefault()

    this.setState(A1(e))
  }
  
  handleSubmit = e => {
    e.preventDefault()
    console.log(this.props)
    this.props.signUp(this.state)
  }
  render() {
    console.log(this.props)
    this.props.firebase.auth.uid && this.props.history.push('/')

    const {authError} = this.props.auth
    return (
      <div className="container">
        <form 
          onSubmit = {this.handleSubmit}
          className = "white"
        >
        <h5 className="grey-text text-darken-3">
          Sign Up
        </h5>

        <div className="input-field">
          <label htmlFor="email">Email</label>
          <input 
            type="email"
            onChange = {this.handleChange}
            id = 'email'
          />
        </div>

        <div className="input-field">
          <label htmlFor="email">Password</label>
          <input 
            type="password"
            onChange = {this.handleChange}
            id = 'password'
          />
        </div>

        <div className="input-field">
          <label htmlFor="firstName">First Name</label>
          <input 
            type="text"
            onChange = {this.handleChange}
            id = 'firstName'
          />
        </div>

        <div className="input-field">
          <label htmlFor="lastName">Last Name</label>
          <input 
            type="text"
            onChange = {this.handleChange}
            id = 'lastName'
          />
        </div>
        
        <div className="input-field">
          <button className="btn pink lighten-1 z-depth-0">
          Sign up</button>
        </div>


        {authError && <p className = 'red-text center-align'> {authError} </p> }
        </form>
      </div>
    )
  }
}

const mstp = s => ({...s})

const mdtp = d => {
  return {
    signUp: newUser => d(signUp(newUser))
  }
}

export default connect(mstp, mdtp)(SignUp)
