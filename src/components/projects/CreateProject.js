import React, { Component } from 'react'

import A1 from "./../helperfunctions/form-onchange";

import A2 from "./../../store/actions/projectActions";

import { connect as A3 } from "react-redux";

class CreateProject extends Component {
  state = {
    title: '',
    content: '',
  }

  handleChange = e => {
    e.preventDefault()

    this.setState(A1(e))
  }
  
  handleSubmit = e => {
    e.preventDefault()
    // console.log(this.state)
    this.props.createProject(this.state)
    this.props.history.push('/')
  }
  render() {

    
    if(!this.props.auth.uid) this.props.history.push('/signin')
    return (
      <div className="container">
        <form 
          onSubmit = {this.handleSubmit}
          className = "white"
        >
        <h5 className="grey-text text-darken-3">
          Create New Project
        </h5>

        <div className="input-field">
          <label htmlFor="title">Title</label>
          <input 
            type="text"
            onChange = {this.handleChange}
            id = 'title'
          />
        </div>

        <div className="input-field">
          <label htmlFor="content">Content</label>
          <textarea 
            id="content"
            onChange = {this.handleChange}
            className = "materialize-textarea"
          >
          </textarea>
        </div>
        
        <div className="input-field">
          <button className="btn pink lighten-1 z-depth-0">
          Create Project</button>
        </div>

        </form>
      </div>
    )
  }
}

const mstp = stt => {
  return {
    auth: stt.firebase.auth
  }
}

const mdtp = d => {
  return {
    createProject: p => d(A2(p))
  }
}

export default A3(mstp, mdtp)(CreateProject)
