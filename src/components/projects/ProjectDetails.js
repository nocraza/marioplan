import React from 'react'

import { connect as A1 } from "react-redux";
import { firestoreConnect as A2 } from "react-redux-firebase";
import { compose as A3 } from "redux";

import { Redirect } from "react-router";

import moment from "moment";

const ProjectDetails = (props) => {
  // console.log(props)
  const {project} = props
  console.log(project)

  if(!props.auth.uid) return <Redirect to = "/signin" />
  if(project){
    return(
      <div className="container section project-details">
        <div className="card z-depth-0">
        <div className="card-content">
          <span className="card-title">
          {project.title} 
          </span>
          <p> {project.content} </p>
        </div>

        <div className="card-action grey lighten-4 grey-text">
          <div>
            Posted by {project.authorFirstName} {project.authorLastName}
          </div>
          <div>
            {moment(project.createdAt.toDate()).fromNow()}
          </div>
        </div>
        </div>
      </div>
    )
  }
  else{
      return (
        <div className="container">
        <p className='center-align'>loading project...</p>
      </div>
    )
  }
}
const mstp = (stt, ownProps) => {
  // console.log('a ', stt)
  const id = ownProps.match.params.id
  const projects = stt.firestore.data.projects
  const project = projects && projects[id]
  return {
    project,
    auth: stt.firebase.auth
    
  }
}

export default A3(
  A1(mstp),
  A2([
    {collection: 'projects'}
  ])
)(ProjectDetails)
