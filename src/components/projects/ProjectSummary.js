import React from 'react'

import { getDayWithSuffix, getMonthName, getTime12HrFormat } from "./../helperfunctions/DateHelpers";

import moment from "moment";

const ProjectSummary = ({project}) => {
  const a1 = project.createdAt.toDate()
  return (
    <div className="card z-depth-0 project-summary">
        <div className="card-content grey-text text-darken-3">
          <span className="card-title">
            {project.title}
          </span>
            <p>Posted by {project.authorFirstName} {project.authorLastName}</p>
            <p className="grey-text">
              {/* {getDayWithSuffix(a1)} of {getMonthName(a1)}, {a1.getFullYear()} @{getTime12HrFormat(a1)} */}
              {moment(a1).fromNow()}
            </p>
          
        </div>
      </div>
  )
}

export default ProjectSummary
