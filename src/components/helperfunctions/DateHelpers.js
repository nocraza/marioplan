export const getMonthName = (d = Date()) => {
  const monthNumber = d.getMonth()
  // console.log(monthNumber)

  let month = ''
  
  switch(monthNumber) {
    case 0:
      month = 'January'
      break
    case 1:
      month = 'February'
      break
    case 2:
      month = 'March'
      break
    case 3:
      month = 'April'
      break
    case 4:
      month = 'May'
      break
    case 5:
      month = 'June'
      break
    case 6:
      month = 'July'
      break
    case 7:
      month = 'August'
      break
    case 8:
      month = 'September'
      break
    case 9:
      month = 'October'
      break
    case 10:
      month = 'November'
      break
    case 11:
      month = 'December'
      break
  }

  return month
  
}

export const getDayWithSuffix = (d = Date()) => {
  const day = d.getDate().toString()
  const g = day[day.length-1]
  let suffix = ''
  
  switch(g) {
    case '1':
      suffix = 'st'
      break
    case '2':
      suffix = 'nd'
      break
    case '3':
      suffix = 'rd'
      break
    default:
      suffix = 'th'
      break
  }
  
  return day+suffix
  
}

export const getTime12HrFormat = (d = Date()) => {
  const hr = d.getHours() + 1
  const b = d.getMinutes()

  return (hr > 12 ? hr-12 : hr)+':'+(b < 10 ? '0'+ b : b)+(hr > 11 && hr !== 24 ? 'PM' : 'AM')
}