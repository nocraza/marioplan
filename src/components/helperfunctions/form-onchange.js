const onChange = e => {
  return {
    [e.target.id]: e.target.value
  }
}

export default onChange