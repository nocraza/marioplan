import React from "react"

import { NavLink as A1 } from "react-router-dom";

const SignedOutLinks = () => {
  return (
    <ul
      className = "right"
    >
      <li>
        <A1 to='/signup'>
          Signup
        </A1>
      </li>
      <li>
      <A1 to='/signin'>
          Login
        </A1>
      </li>
    </ul>
  )
}

export default SignedOutLinks