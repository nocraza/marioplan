import React from "react"

import { NavLink as A1 } from "react-router-dom";

import { connect as A2 } from "react-redux";

import { signOut as A3 } from "./../../store/actions/authActions";
import { sign } from "crypto";

import { withRouter } from "react-router";

const SignedInLinks = (props) => {
  // console.log('signed in', props)
  return (
    <ul
      className = "right"
    >
    <li>
      <A1 to='/create'>
        New Project
      </A1>
    </li>
    <li>
    <a 
        onClick = {props.signOut}
    >
        Log out
      </a>
    </li>
    <li>
    <A1 to='/'
        className = "btn btn-floating pink lighten-1"
      >
        {props.initials}
      </A1>
    </li>
    </ul>
  )
}

const signOut = e => {
  // e.preventDefault()
  // signOut()
  // props.history.push('/')
}

const mstp = stt => {
  return {
    history: stt.history
  }
}

const mdtp = (d, o) => {
  return {
    signOut: () => {d(A3());o.history.push('/signin')}
  }
}

export default withRouter(A2(mstp, mdtp)(SignedInLinks))