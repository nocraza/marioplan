import React from "react"

import { Link as A1 } from "react-router-dom";
import A2 from "./SignedInLinks";
import A3 from "./SignedOutLinks";

import { connect as A4 } from "react-redux";

import './Navbar.scss'

const Navbar = (props) => {
  console.log(props)
  const {auth} = props
  // console.log(auth)
  return (
    <nav
      className = "nav-wrapper marioPlan-nav"
    >
      <div className="container">
        <A1 className="brand-logo" to = '/'>
          MarioPlan
        </A1>

        {auth.uid ? <A2 initials = {props.profile.initials} /> : <A3 />}

      </div>
    </nav>
  )
}

const mstp = stt =>{
  console.log('navbar mstp', stt)
  return {
    auth: stt.firebase.auth,
    profile: stt.firebase.profile
  }
}

export default A4(mstp)(Navbar)