import React, { Component } from 'react'

import A1 from "./Notifications";
import A2 from "./../projects/ProjectList";

import { connect as A3 } from "react-redux";

import { firestoreConnect as A4 } from "react-redux-firebase";

import { Redirect as A5 } from "react-router-dom";

import { compose } from "redux";


class Dashboard extends Component {
  render() {
    // console.log(this.props)
    const { projects, notifications } = this.props
    
    // if(!this.props.auth.uid) {this.props.history.push('/signin')}
    if(!this.props.auth.uid)return <A5 to= '/signin' />
    

      return (
        <div className="dashboard container">
          <div className="row">
            <div className="col s12 m6">
              <A2 
                projects = {projects}
                />
            </div>

            <div className="col s12 m5 offset-m1">
              <A1 notifications = {notifications} />
            </div>
          </div>
        </div>
      )
    
  }
}

const mstp = stt => {
  // console.log(stt)
  return {
    projects: stt.firestore.ordered.projects,
    auth: stt.firebase.auth,
    notifications: stt.firestore.ordered.notifications
  }
}

export default compose(
  A3(mstp),
  A4([
    {collection: 'projects',
      orderBy: ['createdAt', 'desc']
    },
    {collection: 'notifications', 
      limit: 3,
      orderBy: ['time', 'desc']
    }
  ])
)(Dashboard)
