import { combineReducers as A } from "redux";

import B from "./authReducer";
import C from "./projectReducer";

import { firestoreReducer as D } from "redux-firestore";
import { firebaseReducer as E } from "react-redux-firebase";

const rootReducer = A({
  auth: B,
  project: C,
  firestore: D,
  firebase: E
})


export default rootReducer