const initState = {
  authError: null
}

const authReducer = (state = initState, action) => {
  switch (action.type) {
    case 'LOGIN_ERROR':
      return {
        ...state,
        authError: 'Login failed'
      }
      break;
    case 'LOGIN_SUCCESS':
      // console.log('login success')
      return {
        ...state,
        authError: null
      }
    case "SIGNUP_SUCCESS":
      return {
        ...state,
        authError: null
      }
    case "SIGNUP_ERROR":
      return {
        ...state,
        authError: action.err.message
      }
    case 'SIGNOUT_SUCCESS':
      console.log('signout success')
      return state
    default:
      return state
      break;
  }
  
}

export default authReducer