import { withRouter, Redirect } from "react-router-dom";
// import 

export const signin = (credentials) => {
  return (dispatch, getState, {getFirebase}) => {
    const firebase = getFirebase()

    firebase.auth().signInWithEmailAndPassword(
      credentials.email,
      credentials.password
    )
    .then(() => {
      dispatch({
        type: 'LOGIN_SUCCESS'
      })

      window.M.toast({
        html: "Successfully signed in!",
        classes: "rounded pink lighten-1 white-text"
      })
      // window.location.replace('/')
      // console.log(Redirect)
      // Redirect().to = '/'
    })
    .catch(error => {
      console.log(error)
      dispatch({
        type: 'LOGIN_ERROR',
        error
      })
    })
  }
}

export const signOut = () => {
  return (dispatch, getState, {getFirebase}) => {
    const firebase = getFirebase()

    firebase.auth().signOut()
      .then(() => {
        dispatch({type: 'SIGNOUT_SUCCESS'})

        window.M.toast({
          html: "Successfully signed out!",
          classes: "rounded pink lighten-1 white-text"
        })

        // window.location.replace('/signin')
      })
  }
}

export const signUp = newUser => {
  return (dispatch, getState, {getFirebase, getFirestore}) => {
    const firebase = getFirebase()
    const firestore = getFirestore()

    firebase.auth().createUserWithEmailAndPassword(
      newUser.email,
      newUser.password
    )
      .then(resp => {
        return firestore.collection('Users').doc(resp.user.uid)
          .set({
            firstName: newUser.firstName,
            lastName: newUser.lastName,
            initials: newUser.firstName[0] + newUser.lastName[0]
          })
      })
      .then(() => {
        dispatch({
          type: "SIGNUP_SUCCESS"
        })

        window.M.toast({
          html: "Successfully signed up!",
          classes: "rounded pink lighten-1 white-text"
        })
      })
      .catch(err => {
        dispatch({
          type: "SIGNUP_ERROR",
          err
        })
      })
      
  }
}