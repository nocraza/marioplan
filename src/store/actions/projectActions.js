 const createProject = project => {
  return (dispatch, getState, {getFirebase, getFirestore}) => {


    const firestore = getFirestore()
    const profile = getState().firebase.profile
    const authorId = getState().firebase.auth.uid
    firestore.collection('projects')
      .add({
        ...project,
        authorFirstName: profile.firstName,
        authorLastName: profile.lastName,
        authorId,
        createdAt: new Date()
      })
      .then(() => {
        window.M.toast({
          html: "Successfully added new project!",
          classes: 'rounded pink lighten-1 white-text'
        })
        dispatch({
          type: "CREATE_PROJECT",
          project
        })
        // console.log('asdf')
        // window.location.replace('/')
      })
      .catch((error) => {
          dispatch({
            type: 'CREATE_PROJECT_ERROR',
            error
          })
      })
    
  }
}

export default createProject